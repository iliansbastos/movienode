const mongoose = require('mongoose')
const logger = require('../config/logger').getLogger('movienode')

const dbConnection = async () => {

    try {
        let options = {
            dbName: process.env.DB_NAME,
            user: process.env.USER_DB,
            pass: process.env.PASS_DB,
        }
        await mongoose.connect(process.env.MONGODB_CNN, options)

        logger.info("Base de datos conectada")
    } catch (error) {
        throw logger.error('Error al iniciar bd ', error)
    }

}


module.exports = {
    dbConnection
}