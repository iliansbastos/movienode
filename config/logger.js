const log4js = require('koa-log4');

log4js.configure({
    appenders: {
        console: { type: 'console' },
        file: { type: 'dateFile', filename: 'movienode.log', compress: true },
        multilog: { type: 'multiFile', base: 'loglevelmovienode/', property: 'level', extension: '.log', maxLogSize: 10485760, compress: true }
    },
    categories: {
        movienode: { appenders: ['file', 'console', 'multilog'], level: 'info' },
        default: { appenders: ['file', 'console'], level: 'info' }
    }
});

module.exports = log4js