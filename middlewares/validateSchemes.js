const logger = require('../config/logger').getLogger('movienode')
const Scheme = require('../models/movieScheme')

const validateSearchOneMovie = async (ctx, next) => {
    let { year } = ctx.request.header;
    let { title } = ctx.params;
    let { error, value } = Scheme.MovieSearchScheme.validate({ title, year })
    if (error) {
        logger.error("Error de validacion en los datos enviados")
        logger.error(error)
        ctx.status = 400;
        return ctx.body = `Error en los datos enviados`;
    }
    ctx.request.header.year = value.year;
    ctx.params.title = value.title;
    await next()

}

const validateSearchAllMovies = async (ctx, next) => {
    let { page } = ctx.request.header;

    let { error, value } = Scheme.MoviesSearchScheme.validate({ page })
    if (error) {
        logger.error("Error de validacion en los datos enviados")
        logger.error(error)
        ctx.status = 400;
        return ctx.body = "Error en los datos enviados";
    }
    ctx.request.header = value;
    await next()

}
const validateReplaceMovie = async (ctx, next) => {
    let { error, value } = Scheme.MovieReplaceScheme.validate(ctx.request.body)
    if (error) {

        logger.error("Error de validacion en los datos enviados")
        logger.error(error)
        ctx.status = 400;
        return ctx.body = "Error en los datos enviados";
    }
    ctx.request.body = value;
    await next()

}

module.exports = {
    validateSearchOneMovie,
    validateSearchAllMovies,
    validateReplaceMovie
}