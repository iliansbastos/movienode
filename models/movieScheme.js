const Joi = require('joi');

//Decidí cambiar el year a String, ya que la api externa aveces retorna por ejemplo '2008-2020' lo que genera un error.
const MovieSearchScheme = Joi.object().keys({
    title: Joi.string().max(60).min(1).trim().required(),
    year: Joi.string().min(4).max(9).allow('')
})
const MoviesSearchScheme = Joi.object().keys({
    page: Joi.number().min(1)
})

const MovieReplaceScheme = Joi.object().keys({
    movie: Joi.string().max(60).min(1).trim().required(),
    find: Joi.string().max(60).min(1).trim().required(),
    replace: Joi.string().max(60).min(1).trim().required()
})

const Scheme = {
    MovieSearchScheme,
    MoviesSearchScheme,
    MovieReplaceScheme
}

module.exports = Scheme
