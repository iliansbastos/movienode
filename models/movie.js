const { Schema, model } = require('mongoose')

const MovieSchema = Schema({
    Title: {
        type: String,
        required: true,
        unique: true
    },
    Year: {
        type: String,
        required: true,
    },
    Released: {
        type: String,
        required: true
    },
    Genre: {
        type: String,
        required: true
    },
    Director: {
        type: String,
        required: true
    },
    Actors: {
        type: String,
        required: true
    },
    Plot: {
        type: String,
        required: true
    },
    Ratings: {
        type: Schema.Types.Array,
        required: true
    },

})

MovieSchema.methods.toJSON = function () {
    const { __v, _id, ...movie } = this.toObject();
    // usuario.uid = _id;
    return movie;
}

module.exports = model('Movie', MovieSchema)