const Koa = require('koa');
const koaBody = require('koa-body');
const cors = require('@koa/cors');
const log4js = require('koa-log4')
require("@babel/register");
const swaggerUI = require('swagger-ui-koa');
const convert = require('koa-convert');
const mount = require('koa-mount');

const YAML = require('yamljs');

const logger = require('../config/logger').getLogger('movienode')
const swaggerDocument = YAML.load('./doc/swagger.yaml');
const moviesRouter = require('../routes/movies');
const { dbConnection } = require('../config/db');

class Server {

    constructor() {
        this.app = new Koa();

        //logger
        this.app.use(log4js.koaLogger(log4js.getLogger('movienode'), { level: 'auto' }));

        this.port = process.env.PORT || 4000;

        //conectar a bd
        this.conectarDB()

        //middlewares
        this.middlewares();

        //rutas
        this.routes()

    }

    async conectarDB() {
        await dbConnection()
    }

    middlewares() {

        //CORS
        this.app.use(cors())

        //Parsea el body
        this.app.use(koaBody());

        //Control de errores
        this.app.use(async function handleError(context, next) {
            try {
                await next();
            } catch (error) {
                logger.fatal('Error interno 500 ', error)
                context.status = 500;
                context.body = error;
            }
        });

    }

    routes() {

        this.app.use(moviesRouter.routes())
        this.app.use(moviesRouter.allowedMethods());

        //Ruta donde se monta la documentación con swagger
        this.app.use(swaggerUI.serve);
        this.app.use(convert(mount('/doc', swaggerUI.setup(swaggerDocument))));

    }

    listen() {
        this.app.listen(this.port, () => {
            logger.info('Servidor arriba en puerto ', this.port)
            logger.info('http://localhost:' + this.port)
            logger.info('Documentación en la siguiente ruta')
            logger.info(`http://localhost:${this.port}/doc`)
        })
    }

}


module.exports = Server;