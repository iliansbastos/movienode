const request = require('request');
const Movie = require('../models/movie')

/**
 * Consulta a la api omdbapi por el titulo y año de una película,
 * si obtiene un resultado, busca en la BD, si no existe, guarda la información,
 * Luego retorna el resultado guardado
 * 
 * @param {Koa Context} ctx 
 * @returns {Object} Película encontrada
 */
const searchMovie = async (ctx) => {
    let { year } = ctx.request.header;
    let { title } = ctx.params;
    const url = 'https://www.omdbapi.com/?';
    title = title.toLowerCase();

    const { success, data } = await searchMovieApi(title, year, url);
    if (!success) {
        ctx.status = 400;
        return ctx.body = `Película ${title}, no encontrada`;
    }
    const { movie, created } = await searchAndSaveDB(data);
    created ? ctx.status = 201 : ctx.status = 200;
    return ctx.body = movie;
}

/**
 * Busca todas las peliculas en la DB, se página de 5 en 5, y se retorna las películas en la página
 * indicada en caso de haber mas de 5 resultados.
 * 
 * @param {Koa Context} ctx 
 * @returns {Array}
 */
const searchAllMovies = async (ctx) => {
    let { page = 1 } = ctx.request.header;
    const intems = 5;//Items por página

    page = (page - 1) * intems;

    const movieDB = await Movie.find()
        .skip(page)
        .limit(intems);
    return ctx.body = movieDB;
}

/**
 * Busca una película, extrae su campo plot, y reemplaza las palabras encontradas que sean igual al valor
 * enviado en la variable find por la de replace
 * @param {Koa Context} ctx 
 * @returns {String}
 */
const replaceMovie = async (ctx) => {

    let { movie, find, replace } = ctx.request.body;

    const movieDB = await Movie.findOne({ "Title": movie })

    if (!movieDB) {
        ctx.status = 400;
        return ctx.body = `Película ${movie}, no encontrada`;
    }
    let { Plot } = movieDB;
    Plot = Plot.split(find).join(replace);

    ctx.body = Plot;

}

/**
 * Consulta a la api omdbapi por año y year (opcional) una película, devuelve la información
 * en caso de obtener un resultado
 * 
 * @param {String} title 
 * @param {Number} year (Opcional)
 * @param {String} url 
 * @returns {Object}
 */
const searchMovieApi = (title, year, url) => {

    return new Promise((resolve) => {

        title = `t=${title}`;
        year = `&y=${year}`;

        request(`${url + title + year}&apikey=${process.env.OMDB_APIKEY}`, { json: true }, async (err, res, body) => {
            if (err) { return resolve({ success: false, msg: err }) }

            const { Error } = body;
            if (Error) {
                return resolve({ success: false });
            }
            resolve({ success: true, data: body });
        });

    })

}

/**
 * Busca en la bd una película, si no existe la guarda, y devuelve la información encontrada o guardada.
 * 
 * @param {Object} body 
 * @returns {Object} 
 */
const searchAndSaveDB = async (body) => {
    //Busca la pelicula
    const movieDB = await Movie.findOne({ "Title": body.Title.toLowerCase(), "Year": body.Year });
    if (!movieDB) {
        //si no existe la guarda
        let movie = new Movie(body);

        movie.Title = movie.Title.toLowerCase();
        movie.Plot = movie.Plot.toLowerCase();

        //Guardar DB
        await movie.save();
        return { movie, created: true };
    }
    //Devuelve el resultado
    return { movie: movieDB };
}


module.exports = {
    searchMovie,
    searchAllMovies,
    replaceMovie
}