'use stricts'
require('dotenv').config()
let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:' + process.env.PORT;
console.log('url ', url)
const r_prefix = '/api/'

describe('R1 - Consulta y guarda una película: ', () => {

    it('Consulta por título en api omdbapi y guarda en db, verifica estructura objeto devuelto', (done) => {
        chai.request(url)
            .get(`${r_prefix}movie/star wars`)
            .end(function (err, res) {
                expect(res.body).to.have.property('Title');
                expect(res.body).to.have.property('Year');
                expect(res.body).to.have.property('Released');
                expect(res.body).to.have.property('Genre');
                expect(res.body).to.have.property('Director');
                expect(res.body).to.have.property('Actors');
                expect(res.body).to.have.property('Plot');
                expect(res.body).to.have.property('Ratings');
                expect(res).to.have.status(201);
                done();
            });
    });
    it('Prueba de formato erroneo del campo year enviado en el header', (done) => {
        chai.request(url)
            .get(`${r_prefix}movie/star wars`)
            .set('year', '2008-20099')
            .end(function (err, res) {
                expect(res).to.have.status(400);
                done();
            });
    });
    it('Envío de variable title en url y year en el header correctos', (done) => {
        chai.request(url)
            .get(`${r_prefix}movie/star wars`)
            .set('year', '2009')
            .end(function (err, res) {
                expect(res).to.have.status(201);
                done();
            });
    });
});

describe('R2 - Busca y pagina las peliculas obtenidas de 5 en 5 ', () => {

    it('Verifica que el resultado sea un array con algún elemento con status 200', (done) => {
        chai.request(url)
            .get(`${r_prefix}movies/`)
            .end(function (err, res) {
                expect(res.body.length).to.be.at.least(0)
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Verifica que una página no tenga mas de 5 items', (done) => {
        chai.request(url)
            .get(`${r_prefix}movies/`)
            .end(function (err, res) {
                console.log(res.body.length)
                expect(res.body.length).to.be.at.most(5)
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Prueba de formato erroneo del campo page enviado en el header', (done) => {
        chai.request(url)
            .get(`${r_prefix}movies/`)
            .set('page', 'no se')
            .end(function (err, res) {
                expect(res).to.have.status(400);
                done();
            });
    });
});


describe('R3 - Busca y reemplaza palabras del campo Plot de una película', () => {

    it('Verifica estructura del body en el post', (done) => {
        chai.request(url)
            .post(`${r_prefix}movie/`)
            .send({})
            .end(function (err, res) {
                expect(res).to.have.status(400);
                done();
            });
    });
    it('Prueba el flujo correcto', (done) => {
        chai.request(url)
            .post(`${r_prefix}movie/`)
            .send({
                "movie": "star wars",
                "find": "jedi",
                "replace": "CLM Dev"
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                done();
            });
    });

});