const Router = require("koa-router");

const { searchMovie, searchAllMovies, replaceMovie } = require("../controllers/movies");
const { validateSearchOneMovie, validateSearchAllMovies, validateReplaceMovie } = require("../middlewares/validateSchemes");


const router = new Router({
    prefix: '/api/'
});


router.get('movie/:title', validateSearchOneMovie, searchMovie);

router.get('movies/', validateSearchAllMovies, searchAllMovies);

router.post('movie/', validateReplaceMovie, replaceMovie);


module.exports = router;