# Movie Node

Microservicio con 3 endpoints: Busca películas, Obtiene todas las películas, Busca y reemplaza.

## Instalación

Clonar el proyecto desde git.

```bash
git clone https://gitlab.com/iliansbastos/movienode.git
```
Ubicarse dentro de la carpeta del proyecto.

```bash
cd movienode
```

Use npm para instalar todas las dependencias.

```bash
npm install
```

Renombre el archivo .example.env a .env ,agregue los parámetros necesario, luego ejecutar con el siguiente comando, y ¡listo!

```bash
node app
```